# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2012, 2013, 2014, 2015, 2017.
# Lukáš Tinkl <ltinkl@redhat.com>, 2010, 2011.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2012, 2013.
# SPDX-FileCopyrightText: 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 Vit Pelcak <vit@pelcak.org>
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-13 00:39+0000\n"
"PO-Revision-Date: 2024-06-18 16:10+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 24.05.1\n"
"X-Language: cs_CZ\n"
"X-Source-Language: en_US\n"

#: kcmrules.cpp:228
#, kde-format
msgid "Copy of %1"
msgstr "Kopie %1"

#: kcmrules.cpp:408
#, kde-format
msgid "Application settings for %1"
msgstr "Nastavení aplikace pro %1"

#: kcmrules.cpp:430 rulesmodel.cpp:221
#, kde-format
msgid "Window settings for %1"
msgstr "Nastavení okna pro %1"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "Nedůležité"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "Přesná shoda"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "Shoda podřetězce"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "Regulární výraz"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "Použít na začátku"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "Aplikovat nyní"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "Zapamatovat si"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "Nemá vliv"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "Vynutit"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr "Vlastnost okna bude vždy vynucena na danou hodnotu."

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "Dočasně vynutit"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""

#: rulesmodel.cpp:224
#, kde-format
msgid "Settings for %1"
msgstr "Nastavení pro %1"

#: rulesmodel.cpp:227
#, kde-format
msgid "New window settings"
msgstr "Nové nastavení okna"

#: rulesmodel.cpp:243
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""
"Nastavili jste třídu okna jako nedůležitou.\n"
"Tato nastavení se pravděpodobně použijí na všechna okna aplikací. Pokud si "
"skutečně přejete vytvořit generické nastavení, je doporučeno omezit alespoň "
"typ oken a zabránit tak zvláštním typům oken."

#: rulesmodel.cpp:250
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""
"Některé aplikace nastavují vlastní rozměry po spuštění, čímž přepíší vaše "
"původní nastavení pro velikost a umístění. Abyste tato nastavení vynutili, "
"vynuťte taky vlastnost \"%1\" na \"Ano\"."

#: rulesmodel.cpp:257
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""

#: rulesmodel.cpp:388
#, kde-format
msgid "Description"
msgstr "Popis"

#: rulesmodel.cpp:388 rulesmodel.cpp:396 rulesmodel.cpp:404 rulesmodel.cpp:411
#: rulesmodel.cpp:417 rulesmodel.cpp:425 rulesmodel.cpp:430 rulesmodel.cpp:436
#, kde-format
msgid "Window matching"
msgstr "Shoda okna"

#: rulesmodel.cpp:396
#, kde-format
msgid "Window class (application)"
msgstr "Třída okna (aplikace)"

#: rulesmodel.cpp:404
#, kde-format
msgid "Match whole window class"
msgstr "Odpovídá celé třídě okna"

#: rulesmodel.cpp:411
#, kde-format
msgid "Whole window class"
msgstr "Celá třída okna"

#: rulesmodel.cpp:417
#, kde-format
msgid "Window types"
msgstr "Typy oken"

#: rulesmodel.cpp:425
#, kde-format
msgid "Window role"
msgstr "Role okna"

#: rulesmodel.cpp:430
#, kde-format
msgid "Window title"
msgstr "Titulek okna"

#: rulesmodel.cpp:436
#, kde-format
msgid "Machine (hostname)"
msgstr "Počítač (název)"

#: rulesmodel.cpp:442
#, kde-format
msgid "Position"
msgstr "Pozice"

#: rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:454 rulesmodel.cpp:459
#: rulesmodel.cpp:467 rulesmodel.cpp:473 rulesmodel.cpp:492 rulesmodel.cpp:508
#: rulesmodel.cpp:513 rulesmodel.cpp:518 rulesmodel.cpp:523 rulesmodel.cpp:528
#: rulesmodel.cpp:537 rulesmodel.cpp:552 rulesmodel.cpp:557 rulesmodel.cpp:562
#, kde-format
msgid "Size & Position"
msgstr "Velikost a umístění"

#: rulesmodel.cpp:448
#, kde-format
msgid "Size"
msgstr "Velikost"

#: rulesmodel.cpp:454
#, kde-format
msgid "Maximized horizontally"
msgstr "Maximalizované vodorovně"

#: rulesmodel.cpp:459
#, kde-format
msgid "Maximized vertically"
msgstr "Maximalizované svisle"

#: rulesmodel.cpp:467
#, kde-format
msgid "Virtual Desktop"
msgstr "Virtuální plocha"

#: rulesmodel.cpp:473
#, kde-format
msgid "Virtual Desktops"
msgstr "Virtuální plochy"

#: rulesmodel.cpp:492
#, kde-format
msgid "Activities"
msgstr "Aktivity"

#: rulesmodel.cpp:508
#, kde-format
msgid "Screen"
msgstr "Obrazovka"

#: rulesmodel.cpp:513 rulesmodel.cpp:927
#, kde-format
msgid "Fullscreen"
msgstr "Celá obrazovka"

#: rulesmodel.cpp:518
#, kde-format
msgid "Minimized"
msgstr "Minimalizované"

#: rulesmodel.cpp:523
#, kde-format
msgid "Shaded"
msgstr "Sbalené"

#: rulesmodel.cpp:528
#, kde-format
msgid "Initial placement"
msgstr "Úvodní umístění"

#: rulesmodel.cpp:537
#, kde-format
msgid "Ignore requested geometry"
msgstr "Ignorovat požadovanou geometrii"

#: rulesmodel.cpp:540
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""

#: rulesmodel.cpp:552
#, kde-format
msgid "Minimum Size"
msgstr "Minimální velikost"

#: rulesmodel.cpp:557
#, kde-format
msgid "Maximum Size"
msgstr "Maximální velikost"

#: rulesmodel.cpp:562
#, kde-format
msgid "Obey geometry restrictions"
msgstr "Řídit se omezenou geometrií"

#: rulesmodel.cpp:564
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""

#: rulesmodel.cpp:575
#, kde-format
msgid "Keep above other windows"
msgstr "Ponechat nad jinými okny"

#: rulesmodel.cpp:575 rulesmodel.cpp:580 rulesmodel.cpp:585 rulesmodel.cpp:591
#: rulesmodel.cpp:597 rulesmodel.cpp:603
#, kde-format
msgid "Arrangement & Access"
msgstr "Uspořádání a přístup"

#: rulesmodel.cpp:580
#, kde-format
msgid "Keep below other windows"
msgstr "Podržet pod ostatními okny"

#: rulesmodel.cpp:585
#, kde-format
msgid "Skip taskbar"
msgstr "Přeskočit panel úloh"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr ""

#: rulesmodel.cpp:591
#, kde-format
msgid "Skip pager"
msgstr "Přeskočit přepínač ploch"

#: rulesmodel.cpp:593
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""

#: rulesmodel.cpp:597
#, kde-format
msgid "Skip switcher"
msgstr "Přeskočit přepínač"

#: rulesmodel.cpp:599
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""

#: rulesmodel.cpp:603
#, kde-format
msgid "Shortcut"
msgstr "Zkratka"

#: rulesmodel.cpp:609
#, kde-format
msgid "No titlebar and frame"
msgstr "Bez titulku a rámu"

#: rulesmodel.cpp:609 rulesmodel.cpp:614 rulesmodel.cpp:620 rulesmodel.cpp:625
#: rulesmodel.cpp:631 rulesmodel.cpp:658 rulesmodel.cpp:686 rulesmodel.cpp:692
#: rulesmodel.cpp:704 rulesmodel.cpp:709 rulesmodel.cpp:714 rulesmodel.cpp:719
#: rulesmodel.cpp:725
#, kde-format
msgid "Appearance & Fixes"
msgstr "Vzhled a opravy"

#: rulesmodel.cpp:614
#, kde-format
msgid "Titlebar color scheme"
msgstr "Barevné schéma titulkového pruhu"

#: rulesmodel.cpp:620
#, kde-format
msgid "Active opacity"
msgstr "Aktivní krytí"

#: rulesmodel.cpp:625
#, kde-format
msgid "Inactive opacity"
msgstr "Neaktivní krytí"

#: rulesmodel.cpp:631
#, kde-format
msgid "Focus stealing prevention"
msgstr "Prevence ztráty zaměření"

#: rulesmodel.cpp:633
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""

#: rulesmodel.cpp:658
#, kde-format
msgid "Focus protection"
msgstr "Ochrana zaměření"

#: rulesmodel.cpp:660
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""

#: rulesmodel.cpp:686
#, kde-format
msgid "Accept focus"
msgstr "Přijmout zaměření"

#: rulesmodel.cpp:688
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr ""

#: rulesmodel.cpp:692
#, kde-format
msgid "Ignore global shortcuts"
msgstr "Ignorovat globální zkratky"

#: rulesmodel.cpp:694
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""

#: rulesmodel.cpp:704
#, kde-format
msgid "Closeable"
msgstr "Lze zavřít"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr "Název souboru plochy"

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr "Zablokovat kompozici"

#: rulesmodel.cpp:719
#, kde-format
msgid "Layer"
msgstr "Vrstva"

#: rulesmodel.cpp:725
#, kde-format
msgid "Adaptive Sync"
msgstr "Adaptivní synchronizace"

#: rulesmodel.cpp:777
#, kde-format
msgid "Window class not available"
msgstr "Třída okna není dostupná"

#: rulesmodel.cpp:778
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""

#: rulesmodel.cpp:812
#, kde-format
msgid "All Window Types"
msgstr "Všechny typy oken"

#: rulesmodel.cpp:813
#, kde-format
msgid "Normal Window"
msgstr "Normální okno"

#: rulesmodel.cpp:814
#, kde-format
msgid "Dialog Window"
msgstr "Dialogové okno"

#: rulesmodel.cpp:815
#, kde-format
msgid "Utility Window"
msgstr "Nástrojové okno"

#: rulesmodel.cpp:816
#, kde-format
msgid "Dock (panel)"
msgstr "Dok (panel)"

#: rulesmodel.cpp:817
#, kde-format
msgid "Toolbar"
msgstr "Panel nástrojů"

#: rulesmodel.cpp:818
#, kde-format
msgid "Torn-Off Menu"
msgstr "Vytržená nabídka"

#: rulesmodel.cpp:819
#, kde-format
msgid "Splash Screen"
msgstr "Úvodní obrazovka"

#: rulesmodel.cpp:820 rulesmodel.cpp:922
#, kde-format
msgid "Desktop"
msgstr "Pracovní plocha"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:822
#, kde-format
msgid "Standalone Menubar"
msgstr "Samostatná nabídka"

#: rulesmodel.cpp:823
#, kde-format
msgid "On Screen Display"
msgstr "On Screen Display"

#: rulesmodel.cpp:833
#, kde-format
msgid "All Desktops"
msgstr "Všechny plochy"

#: rulesmodel.cpp:835
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr "Zpřístupnit okna na všech plochách"

#: rulesmodel.cpp:854
#, kde-format
msgid "All Activities"
msgstr "Všechny aktivity"

#: rulesmodel.cpp:856
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr "Zpřístupnit okno ve všech aktivitách"

#: rulesmodel.cpp:877
#, kde-format
msgid "Default"
msgstr "Výchozí"

#: rulesmodel.cpp:878
#, kde-format
msgid "No Placement"
msgstr "Bez umístění"

#: rulesmodel.cpp:879
#, kde-format
msgid "Minimal Overlapping"
msgstr "Minimální překrytí"

#: rulesmodel.cpp:880
#, kde-format
msgid "Maximized"
msgstr "Maximalizováno"

#: rulesmodel.cpp:881
#, kde-format
msgid "Centered"
msgstr "Na střed"

#: rulesmodel.cpp:882
#, kde-format
msgid "Random"
msgstr "Náhodné"

#: rulesmodel.cpp:883
#, kde-format
msgid "In Top-Left Corner"
msgstr "V levém horním rohu"

#: rulesmodel.cpp:884
#, kde-format
msgid "Under Mouse"
msgstr "Pod myší"

#: rulesmodel.cpp:885
#, kde-format
msgid "On Main Window"
msgstr "Na hlavním okně"

# žádné parametry funkce v inspektoru funkcí
#: rulesmodel.cpp:892
#, kde-format
msgid "None"
msgstr "Nic"

#: rulesmodel.cpp:893
#, kde-format
msgid "Low"
msgstr "Nízká"

#: rulesmodel.cpp:894 rulesmodel.cpp:924
#, kde-format
msgid "Normal"
msgstr "Normální"

#: rulesmodel.cpp:895
#, kde-format
msgid "High"
msgstr "Vysoká"

#: rulesmodel.cpp:896
#, kde-format
msgid "Extreme"
msgstr "Extrémní"

#: rulesmodel.cpp:923
#, kde-format
msgid "Below"
msgstr "Dole"

#: rulesmodel.cpp:925
#, kde-format
msgid "Above"
msgstr "Nad"

#: rulesmodel.cpp:926
#, kde-format
msgid "Notification"
msgstr "Upozornění"

#: rulesmodel.cpp:928
#, kde-format
msgid "Popup"
msgstr "Rozbalovací"

#: rulesmodel.cpp:929
#, kde-format
msgid "Critical Notification"
msgstr "Kritické upozornění"

#: rulesmodel.cpp:930
#, kde-format
msgid "OSD"
msgstr "OSD"

#: rulesmodel.cpp:931
#, kde-format
msgid "Overlay"
msgstr "Přesah"

#: rulesmodel.cpp:956
#, kde-format
msgid "Unmanaged window"
msgstr "Nespravované okno"

#: rulesmodel.cpp:957
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""

#: ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr "Vybrat soubor"

#: ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr "Pravidla KWinu (*.kwinrule)"

#: ui/main.qml:29
#, kde-format
msgid "Add New…"
msgstr "Přidat nové…"

#: ui/main.qml:35
#, kde-format
msgid "Import…"
msgstr "Importovat…"

#: ui/main.qml:40
#, kde-format
msgid "Cancel Export"
msgstr "Zrušit export"

#: ui/main.qml:40
#, kde-format
msgid "Export…"
msgstr "Exportovat…"

#: ui/main.qml:88
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr "Žádná pravidla pro konkrétní okna nejsou momentálně nastavena"

#: ui/main.qml:89
#, kde-kuit-format
msgctxt "@info"
msgid "Click <interface>Add New…</interface> to add some"
msgstr "Klikněte na tlačítko <interface>Přidat nové...</interface> pro přidání"

#: ui/main.qml:97
#, kde-format
msgid "Select the rules to export"
msgstr ""

#: ui/main.qml:101
#, kde-format
msgid "Unselect All"
msgstr "Zrušit výběr všeho"

#: ui/main.qml:101
#, kde-format
msgid "Select All"
msgstr "Vybrat vše"

#: ui/main.qml:115
#, kde-format
msgid "Save Rules"
msgstr "Uložit pravidla"

#: ui/main.qml:196
#, kde-format
msgid "Edit"
msgstr "Upravit"

#: ui/main.qml:202
#, kde-format
msgid "Duplicate"
msgstr "Duplikovat"

#: ui/main.qml:208
#, kde-format
msgid "Delete"
msgstr "Smazat"

#: ui/main.qml:240
#, kde-format
msgid "Import Rules"
msgstr "Importovat pravidla"

#: ui/main.qml:252
#, kde-format
msgid "Export Rules"
msgstr "Exportovat pravidla"

#: ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr "Nic nevybráno"

#: ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr "Vše označené"

#: ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] "%1 vybráno"
msgstr[1] "%1 vybrány"
msgstr[2] "%1 vybráno"

#: ui/RulesEditor.qml:66
#, kde-format
msgid "No window properties changed"
msgstr "Nebyly změněny žádné vlastnosti okna"

#: ui/RulesEditor.qml:67
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""
"Klikněte na tlačítko <interface>Přidat vlastnost...</interface> níže a "
"přidejte některé vlastnosti okna, které budou pravidlem ovlivněny"

#: ui/RulesEditor.qml:88
#, kde-format
msgid "Close"
msgstr "Zavřít"

#: ui/RulesEditor.qml:88
#, kde-format
msgid "Add Property..."
msgstr "Přidat vlastnost..."

#: ui/RulesEditor.qml:101
#, kde-format
msgid "Detect Window Properties"
msgstr "Detekovat vlastnosti okna"

#: ui/RulesEditor.qml:117 ui/RulesEditor.qml:124
#, kde-format
msgid "Instantly"
msgstr "Okamžitě"

#: ui/RulesEditor.qml:118 ui/RulesEditor.qml:129
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] "Po %1 sekundě"
msgstr[1] "Po %1 sekundách"
msgstr[2] "Po %1 sekundách"

#: ui/RulesEditor.qml:176
#, kde-format
msgid "Add property to the rule"
msgstr "Přidat do pravidla vlastnost"

#: ui/RulesEditor.qml:279 ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr "Ano"

#: ui/RulesEditor.qml:279 ui/ValueEditor.qml:60
#, kde-format
msgid "No"
msgstr "Ne"

#: ui/RulesEditor.qml:281 ui/ValueEditor.qml:168 ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr "%1 %"

#: ui/RulesEditor.qml:283
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: ui/RulesEditor.qml:285
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr "x"
